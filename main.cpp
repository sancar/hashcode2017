#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <set>
#include <assert.h>


long N_VIDEOS;
long N_ENDPOINTS;
long N_REQUESTS;
long N_CACHES;
long CACHE_CAPACITIES;

std::vector<long> VIDEO_SIZES;

std::map<long, std::set<long> > CACHE_TO_VIDEOS; //CACHE to VIDOES
long GAIN_MATRIX[10000][1000];

std::vector<std::vector<long> > ENDPOINTS_FOR_CACHES;

struct endpoint {
    long id;
    long dataCenterLatency;
    long n_connected_caches;
    std::map<long, long> latencyToCache; //cache to latency
};

std::vector<endpoint> ENDPOINTS;
std::map<std::pair<int, int> ,  int> REQ;

std::vector<long> CACHE_SIZE_LEFT;

struct requestDesc {
    long n_requests;
    long video_id;
    long endpoint_id;
};
std::vector<requestDesc> REQUESTS;


std::vector<long> connectedEndpoints(long cache_id) {
    std::vector<long> conEnd;
    for (auto &&endpoint : ENDPOINTS) {
        if (endpoint.latencyToCache.count(cache_id) != 0) {
            conEnd.push_back(endpoint.id);
        }
    }
    return conEnd;
}

std::vector<long> getEndpointsByVideoid(long video_id) {
    std::vector<long> endpoints;
    for (auto &&request :REQUESTS) {
        if (request.video_id == video_id) {
            endpoints.push_back(request.endpoint_id);
        }
    }
    return endpoints;
}

long nRequests(long video_id, long endpoint_id) {
    return REQ[std::make_pair(video_id, endpoint_id)];
}

long latencGain(long endpoint_id, long cache_id, long video_id) {
    endpoint &endp = ENDPOINTS[endpoint_id];
    long diff = endp.dataCenterLatency - endp.latencyToCache[cache_id];
    return diff * nRequests(video_id, endpoint_id);
}

long totalLatencGain(long cache_id, long video_id) {
    std::vector<long> &eids = ENDPOINTS_FOR_CACHES[cache_id];
    long total = 0;

    if (CACHE_SIZE_LEFT[cache_id] < VIDEO_SIZES[video_id]) {
        return 0;
    }

    for (auto &&endp_id : eids) {
        total += latencGain(endp_id, cache_id, video_id);
    }
    return total;
}

//long totalLatencGain(long cache_id, long video_id) {
//    const std::vector<long> &eids = connectedEndpoints(cache_id);
//    long total = 0;
//    for (auto &&endp_id : eids) {
//        total += latencGain(endp_id, cache_id, video_id);
//    }
//    return total;
//}


//void printV(std::vector<long> &&a) {
//    for (auto &&item : a) {
//        std::cout << item << " ";
//    }
//    std::cout << std::endl;
//}
//

long findMax(long &in_videoId, long &in_cacheId) {
    long max = 0;
    for (long videoId = 0; videoId < N_VIDEOS; ++videoId) {
        for (long cacheId = 0; cacheId < N_CACHES; ++cacheId) {
            if (GAIN_MATRIX[videoId][cacheId] > max) {
                max = GAIN_MATRIX[videoId][cacheId];
                in_videoId = videoId;
                in_cacheId = cacheId;
            }
        }
    }
    return max;
}

void updateGainMatrix(long video_id, long cache_id) {
    CACHE_SIZE_LEFT[cache_id] -= VIDEO_SIZES[video_id];

    for (long i = 0; i < N_VIDEOS; ++i) {
        if (CACHE_SIZE_LEFT[cache_id] < VIDEO_SIZES[i]) {
            GAIN_MATRIX[i][cache_id] = 0;
        }
    }

    GAIN_MATRIX[video_id][cache_id] = 0;

    const std::vector<long> &endpoint_ids = getEndpointsByVideoid(video_id);
    for (auto &&endpoint_id : endpoint_ids) {
        for (long i = 0; i < N_CACHES; ++i) {
            GAIN_MATRIX[video_id][i] = GAIN_MATRIX[video_id][i] - latencGain(endpoint_id, cache_id, video_id);
        }
    }
}

void putVideosToCache() {
    long count = 0;
    while (true) {
        count++;
        long videoId, cacheId;
        long max = findMax(videoId, cacheId);
//        std::cout << "b "<< max << " " << videoId << " " << cacheId << std::endl;
        if (max == 0 || count > N_CACHES * N_VIDEOS) {
            break;
        }
//        std::cout << "a " << max << " " << videoId << " " << cacheId << std::endl;
        CACHE_TO_VIDEOS[cacheId].insert(videoId);
        updateGainMatrix(videoId, cacheId);
    }

//    if(CACHE_SIZE_LEFT[]){
//        updateGainMatrix();
//    }
}


//void detailedPrint() {
//    std::cout << "N VIODES " << N_VIDEOS << std::endl;
//    std::cout << "N_ENDPOINTS " << N_ENDPOINTS << std::endl;
//    std::cout << "N_REQUESTS " << N_REQUESTS << std::endl;
//    std::cout << "N_CACHES " << N_CACHES << std::endl;
//    std::cout << "CACHE_CAPACITIES " << CACHE_CAPACITIES << std::endl;
//
//
//    std::cout << "===== VIDEO_SIZES =======" << std::endl;
//    for (long i = 0; i < N_VIDEOS; ++i) {
//        std::cout << i << " id " << VIDEO_SIZES[i] << std::endl;
//    }
//
//    std::cout << std::endl;
//    std::cout << std::endl;
//
//    for (long i = 0; i < N_ENDPOINTS; ++i) {
//        endpoint a = ENDPOINTS[i];
//        std::cout << " endpoint id " << a.id << std::endl;
//        std::cout << " dataCenterLatency  " << a.dataCenterLatency << std::endl;
//        std::cout << " n_connected_caches  " << a.n_connected_caches << std::endl;
//        for (auto &&item : a.latencyToCache) {
//            std::cout << item.first << " " << item.second << std::endl;
//        }
//    }
//
//    for (long i = 0; i < N_REQUESTS; ++i) {
//        std::cout << REQUESTS[i].video_id << " " << REQUESTS[i].endpoint_id << " " << REQUESTS[i].n_requests
//                  << std::endl;
//    }
//}

//void tests() {
//    std::cout << std::endl;
//    std::cout << std::endl;
//    {
//        const std::vector<long> &endpoints = connectedEndpoints(0);
//        for (auto &&item : endpoints) {
//            std::cout << item << " ";
//        }
//        std::cout << std::endl;
//        std::cout << std::endl;
//    }
//    {
//        const std::vector<long> &endpoints = connectedEndpoints(1);
//        for (auto &&item : endpoints) {
//            std::cout << item << " ";
//        }
//        std::cout << std::endl;
//        std::cout << std::endl;
//    }
//    {
//        const std::vector<long> &endpoints = connectedEndpoints(2);
//        for (auto &&item : endpoints) {
//            std::cout << item << " ";
//        }
//        std::cout << std::endl;
//        std::cout << std::endl;
//    }
//    std::cout << isEndpointConncetedToCache(0, 0) << std::endl;
//    std::cout << isEndpointConncetedToCache(0, 1) << std::endl;
//    std::cout << isEndpointConncetedToCache(0, 2) << std::endl;
//    std::cout << isEndpointConncetedToCache(1, 0) << std::endl;
//    std::cout << isEndpointConncetedToCache(1, 1) << std::endl;
//    std::cout << isEndpointConncetedToCache(1, 2) << std::endl;
//
//    std::cout << std::endl;
//    std::cout << std::endl;
//
//    printV(getEndpointsByVideoid(1));
//    printV(getEndpointsByVideoid(3));
//    printV(getEndpointsByVideoid(4));
//    printV(getEndpointsByVideoid(0));
//
//    std::cout << std::endl;
//    std::cout << std::endl;
//
//    std::cout << latencGain(0, 0, 3) << std::endl;
//    std::cout << latencGain(0, 1, 3) << std::endl;
//    std::cout << latencGain(0, 2, 3) << std::endl;
//
//    std::cout << std::endl;
//    std::cout << std::endl;
//    std::cout << totalLatencGain(0, 3) << std::endl;
//}

int main(int c, char **argv) {
    std::ifstream s(argv[1]);
    s >> N_VIDEOS;
    s >> N_ENDPOINTS;
    s >> N_REQUESTS;
    s >> N_CACHES;
    s >> CACHE_CAPACITIES;

    ENDPOINTS_FOR_CACHES.resize(N_CACHES);

    VIDEO_SIZES.resize(N_VIDEOS);
    for (long i = 0; i < N_VIDEOS; ++i) {
        s >> VIDEO_SIZES[i];
    }

    for (long i = 0; i < N_ENDPOINTS; ++i) {
        struct endpoint a;
        a.id = i;
        s >> a.dataCenterLatency;
        s >> a.n_connected_caches;
        std::map<long, long> m;
        for (long j = 0; j < a.n_connected_caches; ++j) {
            long cacheId;
            s >> cacheId;
            long latency;
            s >> latency;
            a.latencyToCache[cacheId] = latency;
            ENDPOINTS_FOR_CACHES[cacheId].push_back(i);
        }
    }

    for (long i = 0; i < N_REQUESTS; ++i) {
        requestDesc rd;
        s >> rd.video_id;
        s >> rd.endpoint_id;
        s >> rd.n_requests;
        REQ[std::make_pair(rd.video_id, rd.endpoint_id)] = rd.n_requests;
        REQUESTS.push_back(rd);
    }

    CACHE_SIZE_LEFT.resize(N_CACHES, CACHE_CAPACITIES);

//    detailedPrint();
//    tests();

    for (long videoId = 0; videoId < N_VIDEOS; ++videoId) {
        for (long cacheId = 0; cacheId < N_CACHES; ++cacheId) {
            GAIN_MATRIX[videoId][cacheId] = totalLatencGain(cacheId, videoId);
        }
    }


//    for (long videoId = 0; videoId < N_VIDEOS; ++videoId) {
//        for (long cacheId = 0; cacheId < N_CACHES; ++cacheId) {
//            std::cout << GAIN_MATRIX[videoId][cacheId] << "\t";
//        }
//        std::cout << std::endl;
//    }
//
//    {
//        long videoId, cacheId;
//        findMax(videoId, cacheId);
//        std::cout << videoId << " " << cacheId << std::endl;
//    }
    putVideosToCache();

    long count = 0;
    for (auto &&item :CACHE_TO_VIDEOS) {
        std::set<long> &videoIds = item.second;
        if(videoIds.size() != 0){
            count++;
        }
    }
    std::cout << count << std::endl;
    for (auto &&item :CACHE_TO_VIDEOS) {
        long cacheId = item.first;
        std::set<long> &videoIds = item.second;
        if(videoIds.size() != 0){
            std::cout << cacheId << " ";
            for (auto &&videoId : videoIds) {
                std::cout << videoId << " ";
            }
            std::cout << std::endl;
        }

    }


    return 0;
}